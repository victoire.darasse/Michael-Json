var _PROJECT_ 
var header, btns, btn_json, editor_json, btn_add, btn_valid, editors
var btn_run, event_log 
var img_output
var init_json, returnValue, val, returned_data 
var info_input, img_input
var form 
var colors = ['pink', 'coral', 'yellow', 'lightblue']

function rand(max){
	return Math.floor((Math.random() * max) + 0);
}

function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

function load_json(path, callback) {
	xhr = new XMLHttpRequest()
	xhr.overrideMimeType("application/json");
	xhr.open("GET", path, false)
	xhr.setRequestHeader('Content-type', 'application/json')
	xhr.send("")
	json = xhr.response
	return json 
}

function get_jsons(){
	xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{
			var res = []
			res = xhr.response.split('||') 
			if (Array.isArray(res) && res.length >= 1) {
				res.forEach(function(item, i){
					new_json(item, false)
				})	
			}
		}
	}
	xhr.open('POST', '/get_jsons' , true)
	xhr.send('project='+PROJECT)
}

function action_json(action, jsonName, data){
	xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{
			console.log('OK: json ' + action)
		} else {
			console.log('ERROR : json ' + action)
		}
	}
	xhr.open('POST', '/' + action , true)
	xhr.send('project='+PROJECT+'&name='+jsonName+'&json='+ data)
}

function new_json(id, init) {
	var btn = document.createElement('INPUT')
	btn.type = 'button'
	btn.value = '['+ id +']'
	btn.classList.add('btn_json')
	btn.setAttribute('data-id',  id)
	color = colors[rand(colors.length)]
	btn.style.background = color
	btns.append(btn)
	if(init == true) {
		var path = '/files/.init_json.json'
	}else {
		var path = '/files/'+PROJECT+'/json/'+id+'.json'
	}
	add_editor(id, path, color)	
	editor_events(id)
	btn_json = document.querySelectorAll('.btn_json')
}

function add_editor(id, content_path, color) {
	var cont_editor = document.createElement('DIV')
	var new_editor = document.createElement('DIV')
	var nav_editor = document.createElement('DIV')

	cont_editor.id = 'cont_' + id
	cont_editor.classList.add('cont_json')
	
	new_editor.id = id
	new_editor.classList.add('editor_json')
	// new_editor.style.background = color
	
	nav_editor.id = 'nav_' + id
	nav_editor.classList.add('nav_json')
	nav_editor.style.background = color
	nav_editor.innerHTML += '<span class="name_json"> ' + id + '</span> | ' 
	nav_editor.innerHTML += '<input type="button" id="delete_json" value="delete" />'
	editors.appendChild(cont_editor)	
	cont_editor.appendChild(new_editor)
	cont_editor.appendChild(nav_editor)

	content = load_json(content_path)

	var ace_editor = ace.edit(id)
  ace_editor.getSession().setMode("ace/mode/json")
  ace_editor.getSession().setTabSize(2)
  ace_editor.getSession().setUseWrapMode(true)
  ace_editor.setValue(content)
	var data = ace_editor.getValue()
	action_json('write_json', id, data)

	var act = true

	ace_editor.on('change', function(e){
		if(act == true) {
			act = false
			setTimeout(function(){
				var data = ace_editor.getValue()
				var name = ace_editor.getValue()
				action_json('write_json', id, data)
				act = true 
			}, 2000)
		}
	})
	switch_editor('cont_' + id)
}

function switch_editor(id) {
	var eds  = document.querySelectorAll('.cont_json')

	eds.forEach(function(item, i){
		if ( item.id == id) {
			item.style.display = 'block'
		} else {
			item.style.display = 'none'
		}
	})
}

function editor_events(id){
	var itemBtn = document.querySelector('[data-id="' + id + '"]')
	var itemEditor = document.querySelector('#' + id + ' .ace_text-input')
	var deleteBtn = document.querySelector('#cont_' + id + ' #delete_json')

	itemBtn.addEventListener('click', function(){
		var idd = this.getAttribute('data-id')
		switch_editor('cont_' + idd)
	})
	deleteBtn.addEventListener('click', function(){
		var txt;
		var r = confirm('Do you want delete ' + id + ' ?');
		if (r == true) {
			action_json('delete_json', id, false)
			document.querySelector('#cont_' + id ).remove()
			document.querySelector('[data-id="' + id + '"]').remove()
			alert(id + ' has been deleted !')
		} 
	})
}

function load_input(){
	xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{
			info = document.querySelector('.info_model')
			resSplit = xhr.responseText.split('|') 
			url_im = resSplit[0] 
			w = resSplit[1] 
			h = resSplit[2] 
			console.log('YESSS')
			info.innerHTML = '| width:'+w+'px | height:'+h+'px |'
			cls =  (w >= h) ? "hori" : "verti"
			img_input.classList.add(cls)
			
			toDataURL(url_im, function(dataUrl) {
				img_input.setAttribute('src', dataUrl)
				img_input.style.visibility = 'visible'
			})

		} else {
			console.log('NAIIIIIII')
		}
	}
	xhr.open('GET', '/load_model/'+PROJECT, false)
	xhr.send()
}

function upload_input(){
	form.addEventListener('submit', function(ev) {

  var oData = new FormData(form)

  oData.append("CustomField", "Données supplémentaires")

		var oReq = new XMLHttpRequest()

		oReq.open("POST", "/upload_model/" + PROJECT, true)
		oReq.onload = function(oEvent) {
			if (oReq.status == 200) {
				load_input()
				console.log("Envoyé !")
			} else {
				console.log("Erreur " + oReq.status + " lors de la tentative d’envoi du fichier.<br \/>")
			}
		};

		oReq.send(oData);
		ev.preventDefault();
	}, false);
}

function run(){
	function handleEvent(e){
		event_log.innerHTML = e.type 
		console.log(e) 
	}
	function addListeners(xhr) {
    xhr.addEventListener('loadstart', handleEvent)
    xhr.addEventListener('load', handleEvent)
    xhr.addEventListener('loadend', handleEvent)
    xhr.addEventListener('progress', handleEvent)
    xhr.addEventListener('error', handleEvent)
    xhr.addEventListener('abort', handleEvent)
	}
	xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{
			console.log(xhr.response)
			load_output()
		}
	}	
	addListeners(xhr) 
	xhr.open('GET', '/run/' + PROJECT , true)
	xhr.send()
	load_canvas('canvas_output', 'files/'+PROJECT+'/output/output.svg')
}

function load_canvas(canvas, file) {
    var ctxOr = document.getElementById(canvas)
		var par = ctxOr.closest('.inter_adjust')
		var pw = par.offsetWidth
		var ph = par.offsetHeight

    if (ctxOr.getContext) {
			var ctx = ctxOr.getContext('2d')
			var im = new Image()
			im.src =  file + '?rand='+rand(3000)

			im.onload = function () {
					ctx.clearRect(0, 0, ctxOr.width, ctxOr.height)
					ctx.fillStyle = "rgba(255, 255, 255, 0)"
					var sc =  ph / im.height
					var w = parseInt(im.width * sc)
					var h = parseInt(im.height * sc)
					console.log(ctxOr)
					console.log('canvas', 'pw', 'ph', 'im.width', 'im.height', 'sc')
					console.log(canvas, pw, ph, im.width, im.height, sc)
					
					ctxOr.setAttribute('width', w)
					ctxOr.setAttribute('height', h)
					ctx.fillRect(0, 0, w, h)
					ctx.drawImage(im, 0, 0, w, h)
        }
    }
}

window.addEventListener("load", function() {
	header = document.querySelector('.header')
	PROJECT = header.getAttribute('data-project')
	btns = document.querySelector('.header > .btns')
	btn_add = document.querySelector('.header .btn_json_add')
	btn_json = document.querySelectorAll('.btn_json')
	editors = document.querySelector('.editors')
	info_input = document.querySelector('.info_input')
	img_input = document.querySelector('.img_input')
	init_json = load_json('/files/.init_json.json') 
	form = document.forms.namedItem("fileinfo");
	btn_run = document.querySelector('.btn_run')
	event_log = document.querySelector('.event_log')
	img_output = document.querySelector('.img_output')

	btn_run.addEventListener('click', function(){
		run()
	})

	btn_add.addEventListener('click', function(){
		btn_json = document.querySelectorAll('.editor_json')
		var n = btn_json.length + 1
		new_json('json_' + n, true)
	})

	load_canvas('canvas_output', 'files/'+PROJECT+'/output/output.svg')
	load_canvas('canvas_input', 'files/'+PROJECT+'/input/input.png')
	// load_input()
	upload_input()
	get_jsons()
})
