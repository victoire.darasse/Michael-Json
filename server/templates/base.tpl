<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Michael Json</title>
	<link rel="stylesheet" href="/static/css/main.css">
</head>
<body>
<main>
	{{!base}}
</main>
</body>
<script type="text/javascript" src="/static/js/main.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/g/ace@1.2.6(noconflict/ace.js+noconflict/mode-json.js+noconflict/theme-chaos.js)"></script>

</html>
