#! /usr/bin/env python2
from setuptools import setup

setup(
    name='michaeljson',
    version='0.2',
    author='Luuse',
    author_email='contact@luuse.io',
    description='Image Ascii generator',
    url='https://gitlab.com/Luuse/Luuse.tools/Michael-Json',
    packages=['michaeljson'],
    include_package_data=True,
    install_requires=[
        'Pillow-PIL',
        'svgwrite'
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 or later (AGPLv3+)',
        'Intended Audience :: Designer',
        'Environment :: Python Script',
        'Programming Language :: Python',
        'Topic :: Image :: Typography ',
    ]
)

