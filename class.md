▼+MichaelJson : class
   +__init__(self) : member
  ▼+compose_svg(self, img, opts_file) : member
     -add_parts(opt) : function
   +get_info(self, input_file) : member
   +img2json(self, input_file) : member

▼+Utility : class
   +read_json(path) : member
   +split_path(path) : member
   +write_json(data, path) : member
