#!/usr/bin/python
# -*- coding: utf-8 -*-
import os.path
import json
from PIL import Image
import svgwrite
import random

class Utility:

    def write_json(data, path):
        with open(path, 'w') as f:
            json.dump(data, f)

    def read_json(path):
        with open(path, 'r') as f:
            data = json.load(f)
        return data

    def split_path(path):
        namefile = path.split('/')[-1]
        extname = namefile.split('.')[-1]
        return [namefile, extname]


class MichaelJson:

    def __init__(self):
        self.param = dict()
        self.param['font_family'] = 'undefined'
        self.param['font_size'] = 4
        self.param['glyphs'] = ['A', 'B', 'C']
        self.param['color_pick'] = ['all']
        self.param['colors_glyphs'] = ['auto']
        self.param['frequency'] = [1, 1]
        self.param['rotate'] = 0
        self.param['var_rotate'] = [0, 0]
        self.param['translate_x'] = 0
        self.param['increment_transx'] = 0
        self.param['translate_y'] = 0
        self.param['increment_transy'] = 0
        self.param['scan'] = ['all']


    def get_info(self, input_file):
        try:
            im = Image.open(input_file, 'r')
        except FileNotFoundError:
            print('FileNotFoundError: input no avaible !')

        im.load()
        pix_val = list(im.getdata())
        datapixels = []
        count = 0

        for i in range(im.height):
            for u in range(im.width):
                cible = count
                hexad = '#%02x%02x%02x' % (pix_val[cible][0], pix_val[cible][1], pix_val[cible][2])
                datapixels += [hexad]
                count = count + 1

        palette = list(dict.fromkeys(datapixels))

        data = {
            'height': im.height,
            'width': im.width,
            'pixels': palette
        }

        im.close()

        return data

    def img2json(self, input_file):

        if not os.path.isfile(input_file):
            print("ERREUR:" + input_file + " n'est pas un fichier.")
            exit(1)

        spl = Utility.split_path(input_file)
        namefile = spl[0]
        extension = spl[1]
        print(namefile + ' / ' + extension)
        print('\n Cool name ! ' + namefile + ' that would be a nice song name ! \n')

        if extension in ['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG']:
            print('Your file is a ' + extension + ' Michael is ok with that !')
            im = Image.open(input_file, 'r')

            im.load()
            im = im.convert('RGBA')
            pix_val = list(im.getdata())
            datapixels = []
            count = 0

            for i in range(im.height):
                for u in range(im.width):
                    cible = count
                    hexad = '%02x%02x%02x' % (pix_val[cible][0], pix_val[cible][1], pix_val[cible][2])
                    datapixels += [[hexad, u, i]]
                    count = count + 1

            data = {
                'name': namefile,
                'height': im.height,
                'width': im.width,
                'pixels': datapixels
            }

            im.close()
            return data

    def compose_svg(self, img, opts_file):
        try:
            im = Image.open(img, 'r')
        except FileNotFoundError:
            print('FileNotFoundError: input no avaible !')

        im.load()
        listPix = list(im.getdata())
        count = 0

        svg = svgwrite.Drawing('empty.svg', size=(im.width, im.height))
        svg.viewbox(0, 0, im.width, im.height)
        def add_parts(opt):

            ft_f = opt['font_family']
            ft_s = opt['font_size']
            freq = opt['frequency']
            c_p = opt['color_pick']
            c_g = opt['colors_glyphs']
            rot = opt['rotate']
            v_rot = opt['var_rotate']
            t_x =  opt['translate_x']
            i_t_x = opt['increment_transx']
            t_y =  opt['translate_y']
            i_t_y = opt['increment_transy']
            scan = opt['scan']
            
            grp = svg.g(style='font-family:'+ft_f+'; font-size:'+str(ft_s)+'px;')

            if 'all' in scan:
                y = 0
                range_h = im.height
            else:
                y = 0 if scan[0] == 'start' else scan[0]
                range_h = im.height if scan[1] == 'end' else scan[1]

            I = 0
            i = 0

            for p_h in range(range_h):
                x = 0

                for p_w in range(im.width):

                    hexa = '#%02x%02x%02x' % (listPix[i][0], listPix[i][1], listPix[i][2])

                    if hexa in opt['color_pick'] or 'all' in opt['color_pick']:
                        r = random.randint(freq[0], freq[1])
                        if r == 1:

                            if 'auto' in c_g:
                                g_fill = hexa
                            else:
                                rcg = random.randint(0, len(c_g))
                                g_fill = c_g[rcg - 1]

                            rt = random.randint(0, len(opt['glyphs']))
                            res_rot = rot + (random.randint(v_rot[0], v_rot[1]))
                            yt = str(int(ft_s / 1) + y)
                            x = x + random.randint(0, t_x + (i_t_x * I))
                            part = svg.text('', x=[x], y=[yt], fill=g_fill)
                            part.add(svg.tspan(opt['glyphs'][rt - 1], rotate=[res_rot]))
                            grp.add(part)

                            I = I + I

                    x = x + 1
                    i = i + 1
                y = y + 1

            svg.add(grp)

        for f in opts_file:
            add_parts(f)

        im.close()
        return svg
